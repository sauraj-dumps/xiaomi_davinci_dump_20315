## aosp_davinci-userdebug 13 TQ2A.230305.008.C1 1679751408 release-keys
- Manufacturer: xiaomi
- Platform: sm6150
- Codename: davinci
- Brand: Xiaomi
- Flavor: aosp_davinci-userdebug
- Release Version: 13
- Kernel Version: 4.14.288
- Id: TQ2A.230305.008.C1
- Incremental: 1679751408
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Xiaomi/davinci/davinci:13/TQ2A.230305.008.C1/1336:userdebug/release-keys
- OTA version: 
- Branch: aosp_davinci-userdebug-13-TQ2A.230305.008.C1-1679751408-release-keys
- Repo: xiaomi_davinci_dump_20315
